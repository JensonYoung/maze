package maze;

public enum MazeTileStatus {
    /**
     * This tile has not been explored
     */
    UNEXPLORED,
    /**
     * This tile has been explored, and is not a solution to the maze
     */
    BADPATH,
    /**
     * This tile has been explored, and is a candidate solution to the maze
     * until the exit is discovered
     */
    GOODPATH,
}
