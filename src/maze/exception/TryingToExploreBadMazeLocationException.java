package maze.exception;

/**
 * An attempt was made to explore a location which is not a space in the maze.
 */
public class TryingToExploreBadMazeLocationException extends Exception {
    public TryingToExploreBadMazeLocationException() {
        super();
    }
}
