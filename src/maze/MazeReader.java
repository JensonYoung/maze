package maze;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import maze.exception.InvalidMazeSizeException;
import maze.exception.InvalidMazeTileException;

public class MazeReader {
    /**
     * Read a maze file consisting of a rectangle of 0 and 1 into a 2D MazeTile
     * array.
     *
     * @param mazeFile Path to text file containing the maze data.
     * @return 2D array of MazeTile
     * @throws IOException
     * @throws InvalidMazeSizeException
     * @throws InvalidMazeTileException
     */
    public static MazeTile[][] readMazeFileToArray(String mazeFile)
            throws IOException, InvalidMazeSizeException, InvalidMazeTileException {
        List<String> mazeFileLines = Files.readAllLines(Paths.get(mazeFile));
        int rows = mazeFileLines.size(); // Each line in the file is a row for the 2D array
        int cols = 0;

        // Determine the required number of columns in the 2D array.
        // This step could be disregarded if the mazes can only be squares.
        for (String line : mazeFileLines) {
            if (line.length() > cols) {
                cols = line.length();
            }
        }

        // Minimum size of 4x4 for a maze to exist
        if (rows <= 3 || cols <= 3) {
            throw new InvalidMazeSizeException();
        }

        // Size requirements determined, now create and fill 2D array.
        // Done this way to avoid using a List; not for a practical purpose
        // except "optimisation"
        MazeTile[][] maze = new MazeTile[rows][cols];

        for (int row = 0; row < rows; row++) {

            int colLen = mazeFileLines.get(row).length();

            for (int col = 0; col < cols; col++) {
                // Don't assume all columns are the same length
                if (col > colLen) {
                    // Void or "unassigned" tiles are treated essentially as walls
                    // but can have other logic applied if need be
                    maze[row][col] = new MazeTileVoid();
                } else {
                    // TODO: Determine which chars will be a wall vs. space in
                    // professor's provided file
                    switch (mazeFileLines.get(row).charAt(col)) {
                        case '0':
                            maze[row][col] = new MazeTileSpace();
                            break;
                        case '1':
                            maze[row][col] = new MazeTileWall();
                            break;
                        default:
                            // If it's not 0 or 1 it's bad input
                            throw new InvalidMazeTileException();
                    }
                }
            }
        }

        return maze;
    }
}
